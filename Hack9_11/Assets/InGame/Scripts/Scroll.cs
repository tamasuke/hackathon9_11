﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour
{
    public GameObject BackTexture;

    public ChangeFloor changeFloor;

    readonly float accel = 0.1f;
	// Use this for initialization
	void Start ()
    {
        changeFloor = GameObject.Find("Canvas").transform.FindChild("FloorChange").GetComponent<ChangeFloor>(); 
	}

	// Update is called once per frame
	void Update ()
    {
        if (changeFloor.getState() == ChangeFloor.FloorState.MAGNET)
        {
            this.transform.position += new Vector3(-accel / 2, 0, 0);
        }
        else
        {
            this.transform.position += new Vector3(-accel, 0, 0);
        }

        if (this.transform.position.x < -20f)
        {
            var instance = Instantiate(BackTexture);
            instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
            DestroyObject(this.gameObject);
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour
{
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        var v = new Vector2(-0.1f, rb.velocity.y);
        transform.Translate(v);
        
        if (transform.position.x <= -10.0f)
        {
            Destroy(this.gameObject);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        //衝突判定
        if (collision.gameObject.tag == "Player")
        {
            //相手のタグがPlayerならば、自分を消す
        }
    }
}

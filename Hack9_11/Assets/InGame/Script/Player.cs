﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Player : MonoBehaviour {

    //運動学
    Vector2 m_position=new Vector2(0,0);
    Vector2 m_speed = new Vector2(0, 0);
    Vector2 m_acceleration = new Vector2(0, -9.8f);

    //初期条件
    Vector2 m_initial_position = new Vector2(0, 0);
    Vector2 m_initial_speed=new Vector2(0,3f);
    Vector2 m_initial_acceleration=new Vector2(0, -9.8f);

    private GameObject m_floor;
    
    //重さ
    float m_weight = 10.0f;
    //半径
    float m_r = 3.0f;

    //障害物の位置
    //Vector2 m_obstacle_position;

    /*
    //collision
    bool m_is_collision=false;
    bool m_is_init_collision = false;
    bool m_is_deinit_collision = false;
    */

    //floor state
    ChangeFloor.FloorState m_floor_state;
    
    //bounce
    bool m_is_bounce=false;
    bool m_is_init_bounce=false;
    bool m_is_deinit_bounce = false;
    int m_bouncing_time=0;

    //slip
    Vector2 m_slip_speed=new Vector2(2.0f,0);

	// Use this for initialization
	void Start (){
        m_position.Set(Screen.width / 2, Screen.height / 2);
        var pos = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 5f));
        pos.z = -1f;

        transform.position = pos;

        m_floor = GameObject.Find("Canvas").transform.FindChild("FloorChange").gameObject;
    }

    /*
    void Collision()
    {
        if (m_is_collision)
        {
            if (!m_is_init_collision)
            {
                m_is_init_collision = true;
                m_is_deinit_collision = false;
                
            }

            m_position.Set(m_obstacle_position.x- m_r
                        , m_obstacle_position.y);


            if (!m_is_deinit_collision)
            {
                m_is_init_collision = false;
                m_is_deinit_collision = true;
            }
        }
    }
    */
    void Magnet()
    {
        m_position.y=m_initial_position.y;
        
    }

    void Slip()
    {
        //y
        if (m_position.y >= m_initial_position.y)
        {
            m_is_bounce = true;
            Bounce();//////////////////////////////////////////////////////////////Bounce()注意！！！！とんでいるときの処理
        }
        else
        {
            DeinitBounce();
            m_position.y = m_initial_position.y;
        }

        //x
        if (m_position.x <= m_initial_position.x)
        {
            m_position.x+=m_slip_speed.x;
        }
        else
        {
            m_position.x = m_initial_position.x;
        }
    }


    void InitBounce()
    {
        m_bouncing_time = 0;

        m_is_init_bounce = true;
        m_is_deinit_bounce = false;
    }

    void DeinitBounce()
    {
        m_position = m_initial_position;

        m_is_init_bounce = false;
        m_is_deinit_bounce = true;

        m_is_bounce = false;
    }

    void Bounce()
    {
        if (m_is_bounce)
        {
            //init
            if (!m_is_init_bounce)
            {
                InitBounce();
            }


            //update move
            m_position.Set(m_initial_position.x+m_initial_speed.x * m_bouncing_time + (1 / 2) * m_initial_acceleration.x * Mathf.Pow(m_bouncing_time, 2f) * m_weight,
                            m_initial_position.y+m_initial_speed.y * m_bouncing_time + (1 / 2) * m_initial_acceleration.y * Mathf.Pow(m_bouncing_time,2f) * m_weight);

            //update time
            m_bouncing_time++;

            //deinit
            if (!m_is_deinit_bounce&&m_position.y<=0)
            {
                DeinitBounce();
            }
        }
    }

    // Update is called once per frame
    void Update () {
        var f = m_floor.transform.GetComponent<ChangeFloor>();


        // Collision();
        switch (m_floor_state)
        {
            case ChangeFloor.FloorState.ICE:Slip();break;
            case ChangeFloor.FloorState.GUM:Bounce(); break;
            case ChangeFloor.FloorState.MAGNET:Magnet(); break;
            default:break;
        }

        var screenPos = Camera.main.WorldToScreenPoint(transform.position);
        if(screenPos.x <= -1f)
        {
            SceneManager.LoadScene("Result");
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Wall") {
        }
    }

    void OnCollisionStay2D(Collision2D other)
    {

        if(other.gameObject.tag == "Enemy1")
        {
            transform.Translate(-0.1f, 0, 0);
        }
    }

    public void AddForce()
    {
        var p = transform.position;
        p.y += 5f;
        var pos = new Vector3[]
        {
            transform.position,
            p,
            transform.position
        };
        transform.DOPath(pos, 1f);

    }
}

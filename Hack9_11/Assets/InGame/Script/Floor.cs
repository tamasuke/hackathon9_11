﻿using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour {

    private GameObject m_floorChanger;

    [SerializeField]
    private Sprite m_ice, m_gum, m_mag;

	// Use this for initialization
	void Start () {
        m_floorChanger = GameObject.Find("Canvas").transform.Find("FloorChange").gameObject;

	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(-0.1f, 0, 0);

        var pos = Camera.main.WorldToScreenPoint(transform.position);
        if (pos.x <= -100f)
            transform.position = new Vector3(3f, -6.3f, -1f);

        var state = m_floorChanger.transform.GetComponent<ChangeFloor>().getState();

        if (state == ChangeFloor.FloorState.GUM)
        {
            transform.FindChild("1").GetComponent<SpriteRenderer>().sprite = m_gum;
            transform.FindChild("2").GetComponent<SpriteRenderer>().sprite = m_gum;
        }
        if (state == ChangeFloor.FloorState.ICE)
        {
            transform.FindChild("1").GetComponent<SpriteRenderer>().sprite = m_ice;
            transform.FindChild("2").GetComponent<SpriteRenderer>().sprite = m_ice;
        }
        if (state == ChangeFloor.FloorState.MAGNET)
        {
            transform.FindChild("1").GetComponent<SpriteRenderer>().sprite = m_mag;
            transform.FindChild("2").GetComponent<SpriteRenderer>().sprite = m_mag;
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class MoveBat : MonoBehaviour
{
    Rigidbody2D rb;

    bool batFlyState = true;

    // Update is called once per frame
    void Start ()
    {
        Vector3 pos = transform.position;
        transform.position = pos;

        rb = GetComponent<Rigidbody2D>();
    }
    void Update ()
    {
        if (transform.position.x <= -10.0f)
        {
            Destroy(this.gameObject);
        }

        if (transform.position.y <= -3.5f)
        {
            batFlyState = false;
        }

        if (transform.position.y > -3.5f && batFlyState == true)
        {
            var v = new Vector2(-0.1f, -0.1f);
            transform.Translate(v);
        }
        else if (batFlyState == false)
        {
            var v = new Vector2(-0.1f, 0.1f);
            transform.Translate(v);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        //衝突判定
        if (collision.gameObject.tag == "Player")
        {
            //相手のタグがPlayerならば、自分を消す
            Destroy(this.gameObject);
        }
    }

}

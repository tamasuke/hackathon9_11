﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour {

    private SpriteRenderer m_back;

    // Use this for initialization
    void Start () {

        GetComponent<SpriteRenderer>()
            .material.DOColor(new Color(1, 1, 1, .1f), 1f)
            .SetLoops(-1, LoopType.Yoyo);

        m_back = transform.FindChild("Fade").GetComponent<SpriteRenderer>();
        m_back.enabled = false;
        m_back.material.color = new Color(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update () {
        var info = InputUtil.GetTouch();
        
        if(info == TouchInfo.Began)
        {
            // ゲームスタート
            m_back.enabled = true;
            m_back.material.DOColor(new Color(0, 0, 0, 1f), 1f)
                .OnComplete(() =>
                {
                    SceneManager.LoadScene("Main");
                });

        }	
	}
}

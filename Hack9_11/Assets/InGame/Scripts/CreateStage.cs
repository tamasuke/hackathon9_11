﻿using UnityEngine;
using System.Collections;

public class CreateStage : MonoBehaviour {

    public GameObject Qube;

    public GameObject Hole;

    public GameObject Enemy;

    public GameObject EnemyBlock;

    public GameObject Obstacle;

    public GameObject Nidle;

    public Player player;

    public enum TrapType
    {
        Qube,
        Hole,
        Enemy,
        Nidle,
        Obstacle
    }

    TrapType rand = (TrapType)Random.Range(1, 4);

    static int m_count = 0;
    // Use this for initialization

    TrapType trapType = TrapType.Qube;

    static readonly int MaxDrawQube = 20;
	
    void Start()
    {
        m_count++;
    }
	// Update is called once per frame
	void Update ()
    {
        if(this.transform.position.x < new Vector3(-Screen.width / 2, 0, 0).x)
        {
            if (m_count > MaxDrawQube)
            {
                if (rand == TrapType.Enemy)
                {
                    var instance = Instantiate(EnemyBlock);
                    instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
                    DestroyObject(this.gameObject);
                }
                else if (rand == TrapType.Hole)
                {
                    var instance = Instantiate(Hole);
                    instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
                    DestroyObject(this.gameObject);
                }
                else if (rand == TrapType.Nidle)
                {
                    var instance = Instantiate(Nidle);
                    instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
                    DestroyObject(this.gameObject);
                }
                else if (rand == TrapType.Obstacle)
                {
                    var instance = Instantiate(Obstacle);
                    instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
                    DestroyObject(this.gameObject);
                }

            }
            else
            {
                var instance = Instantiate(Qube);
                instance.transform.position = new Vector3(/*Screen.width*/20, 0, 0);
                DestroyObject(this.gameObject);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

public class ChangeFloor : MonoBehaviour
{
    // 床の状態
    public enum FloorState
    {
        ICE,        // 氷
        MAGNET,     // 磁石
        GUM         // ゴム
    }

    FloorState m_state = FloorState.ICE;

    public FloorState getState()
    {
        return m_state;
    }

    public void SetIce()
    {
        m_state = FloorState.ICE;
    }

    public void SetGum()
    {
        m_state = FloorState.GUM;
    }

    public void SetMagnet()
    {
        m_state = FloorState.MAGNET;
    }

    void Update()
    {
        switch (m_state)
        {
            case FloorState.ICE:
                Debug.Log("ICE");
                break;

            case FloorState.MAGNET:
                Debug.Log("MAGNET");
                break;

            case FloorState.GUM:
                Debug.Log("GUM");
                break;
        }
    }
}

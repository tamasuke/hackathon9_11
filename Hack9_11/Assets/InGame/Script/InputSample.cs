﻿using UnityEngine;
using System.Collections;

public class InputSample : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var info = InputUtil.GetTouch();

        if (info == TouchInfo.None)
        {

        }
        else if(info == TouchInfo.Began)
        {
            // タッチ座標のスクリーン座標を取得
            var touchPos = InputUtil.GetTouchPosition();
            Debug.Log("began");
        }
        else if(info == TouchInfo.Moved)
        {
            // タッチ座標のワールド座標を取得
            var worldPos = InputUtil.GetTouchWorldPosition(Camera.main);
            Debug.Log("move");
        }
        else if(info == TouchInfo.Ended)
        {
            Debug.Log("end");
        }

	}
}

﻿using UnityEngine;
using System.Collections;

public class SingletonMonobehaviour<T> : MonoBehaviour {

	public static T Instance{	get; private set;	}

	protected bool SetInstance(T instance)

	{

		if( Instance == null)

		{

			Instance = instance;

			return true;

		}else{

			Destroy(this);

			return false;

		}

	}

}